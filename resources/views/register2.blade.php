@extends('layouts.master')
   
@section('content')

@section('header', 'Halaman Form')

<section class="content">
<div class="card">
  <div class="card-header">
    <h3 class="card-title">halaman form</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    <h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">
@csrf
  <label for="first">First Name :</label> <br />
  <input type="text" name="first" /> <br /><br />
  <label for="last">Last Name :</label><br />
  <input type="text" name="last" /><br /><br />
  
  <label for="">Gender:</label><br /><br />
  <input type="radio" id="male" name="gender" value="male" />
  <label for="male">Male</label><br />
  <input type="radio" id="female" name="gender" value="female" />
  <label for="female">Female</label><br />
  <input type="radio" id="other" name="gender" value="other" />
  <label for="other">Other</label><br /><br />

  <label for="national">Nationality</label><br /><br />
  <select name="national" id="">
    <option value="Indonesian">Indonesian</option>
    <option value="Jepang">Jepang</option>
    <option value="China">China</option> </select
  ><br /><br />

  <label for=""></label>
  <label for="">Language Spoken:</label><br /><br />
  <input type="checkbox" id="Bahasa" name="gender" value="Bahasa" />
  <label for="Bahasa">Bahasa Indonesia</label><br />
  <input type="checkbox" id="english" name="gender" value="english" />
  <label for="english">english</label><br />
  <input type="checkbox" id="other" name="gender" value="other" />
  <label for="other">Other</label><br /><br />

  <label for="bio">Bio :</label><br /><br />
  <textarea name="bio" id="" cols="30" rows="10"></textarea>
  <br />
  <button type="submit">Sign Up</button>
</form>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    Footer
  </div>
  <!-- /.card-footer-->
</div>
<!-- /.card -->

</section>
@endsection